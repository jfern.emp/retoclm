const mongoose = require('mongoose')

const Schema = mongoose.Schema

let movieSchema = new Schema({
    Title: { type: 'String', unique: true, uppercase: true },
    Year: String,
    Released: String,
    Genre: String,
    Director: String,
    Actors: String,
    Plot: String,
    Ratings: String
})

const Movie = mongoose.model('Movie', movieSchema)

module.exports = Movie;