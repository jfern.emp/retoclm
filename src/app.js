
const Koa = require('koa');
const app = new Koa();
const logger = require('koa-logger');
const cors = require('koa-cors');
const dotenv = require("dotenv");
const router = require('./routes');
const connect_db = require('./connection/db_connect');
dotenv.config();

app.use(logger())
app.use(cors());

connect_db.then(()=>console.log("db is connected"))
.catch((err)=>console.log(err))

app.use(router.routes())


module.exports = app;
