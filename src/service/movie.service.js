const Movie = require("../schemas/movie");

const saveMovie = async (movie)=> {
    const _movie = Movie(movie)
    const response = await _movie.save();
    return response;
}

const searchMovie = async(title)=> {
    title.Title = title.Title.toUpperCase();
    const response = await Movie.find(title);
    return response;
}

const searchMoviePlot = async(title)=> {
    title.Title = title.Title.toUpperCase();
    const response = await Movie.find(title, "Plot");
    return response;
}

const searchAllMovies = async(page)=> {
    const perPage = 5
    const { Page } = page;
    const response = await Movie.find({}).limit(perPage).skip(perPage * Page);
    return response;
}

module.exports = { saveMovie, searchMovie, searchAllMovies, searchMoviePlot };