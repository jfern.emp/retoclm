const mongoose = require('mongoose');
const db_url = process.env.DB_URL || 'mongodb://localhost:27017/reto' 
const connect_db = mongoose.connect(db_url);

module.exports = connect_db;