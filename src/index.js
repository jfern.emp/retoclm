const app = require('./app');
const port = process.env.PORT || 3000
const main = () => {
    app.listen(port);
    console.log(`App is starting at port: ${port}`)
}

main();