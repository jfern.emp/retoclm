const Router = require('koa-router');
const { saveMovie, searchMovie, searchAllMovies, searchMoviePlot } = require('../service/movie.service');
const validateSchema = require('../routes/validation');
const bodyParser = require('koa-body')();

const router = Router();

router.get('/', (ctx) => {
    ctx.body = "Looking for a movie?";
});


router.get('/movies', async (ctx) => {
    const { Title } = ctx.request.query;
    console.log(Title)
    if (Title !== undefined) {
        const { year } = ctx.header;
        const request = year !== undefined ? Object.assign({ Title: Title }, { Year: year }) : { Title: Title };
        const { error, value } = validateSchema.searchSchema.validate(request);
        if (error) 
            ctx.throw(400, error);
        const [response] = await searchMovie(value)
        ctx.body = response || {};
    } else {
        const { page } = ctx.header;
        const { error, value } = validateSchema.searchMoviesSchema.validate({ Page: Number(page) });
        if (error) 
            ctx.throw(400, error);
        const response = await searchAllMovies(value)
        ctx.body = response;
    }
});


router.post('/movies/replaceplot', bodyParser, async (ctx) => {
    const request = ctx.request.body;
    const { error, value } = validateSchema.replacePlotSchema.validate(request);
    if (error) 
        ctx.throw(400, error);
    const [response] = await searchMoviePlot(value)
    const { find, replace } = request
    const regFind = new RegExp(find, 'g');
    ctx.body = response!==undefined  ?  response.Plot.replace(regFind, replace) : "";
});



router.post('/movies/save', bodyParser, async (ctx) => {
    const movie = ctx.request.body;
    const { error, value } = validateSchema.createSchema.validate(movie);
    if (error) 
        ctx.throw(400, error);
    const response = await saveMovie(value);
    ctx.body = response;
});

router.get('error', '/error', (ctx) => {
    ctx.throw(500, 'internal server error');
});

module.exports = router;