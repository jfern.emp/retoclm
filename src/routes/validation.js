const Joi = require('joi');

const createSchema = Joi.object().keys({
    Title: Joi.string().required(),
    Year: Joi.string().required(),
    Released: Joi.string().required(),
    Genre: Joi.string().required(),
    Director: Joi.string().required(),
    Actors: Joi.string().required(),
    Plot: Joi.string().required(),
    Ratings: Joi.string().required()
});

const searchSchema = Joi.object({
    Title: Joi.string().required(),
    Year: Joi.string()
})

const searchMoviesSchema = Joi.object({
    Page: Joi.number()
});

const replacePlotSchema = Joi.object({
    Title: Joi.string().required(),
    find: Joi.string().required(),
    replace: Joi.string().required()
});

module.exports =  { createSchema, searchSchema, searchMoviesSchema, replacePlotSchema }